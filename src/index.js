import React from 'react';
import ReactDOM from 'react-dom';
import {Router, Route, browserHistory} from 'react-router';
import './index.css';

import Landing from './views/landing';
import Juegos from './views/juegos';
import Score from './views/score-movil';

ReactDOM.render(
	<Router history={browserHistory}>
		<Route path="/" component={Landing}/>
		<Route path="juegos" component={Juegos}/>
		<Route path="score" component={Score}/>
	</Router>,
	document.getElementById('root')
);
