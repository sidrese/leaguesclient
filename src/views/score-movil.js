import React, {Component} from 'react';
import SocketClient from 'socket.io-client';

const socket = SocketClient.connect('http://labs.docademic.com:3006');

class Score extends Component {
	constructor(props) {
		super(props);
		this.state = {
			match: null,
			score_1: 0,
			score_2: 0
		}
	}

	componentDidMount() {
		socket.on('setMatch', (data) => {
			this.setState({match: data});
		});
	}

	syncScore(){
		socket.emit('setScore', {
			score_1: this.state.score_1,
			score_2: this.state.score_2
		});
	}

	render() {
		let match = this.state.match;
		let score_f1 = this.state.score_1.toString().length === 1 ? '0' + this.state.score_1 : this.state.score_1;
		let score_f2 = this.state.score_2.toString().length === 1 ? '0' + this.state.score_2 : this.state.score_2;
		let team_1;
		let team_2;
		let button = <br/>;
		if (match) {
			this.syncScore();
			team_1 = match.team_1.name;
			team_2 = match.team_2.name;
			button = this.state.in_match ?
				<button id="startT" onClick={(event) => this.endMatch()}>
					Finish
				</button> :
				<button id="startT" onClick={(event) => this.startTimer()}>
					start
				</button>
		} else {
			team_1 = 'HOME';
			team_2 = 'GUEST';
		}
		return (
			<div id="score" className="mgn-top">
				<div className="row">
					<div className="col-xs-6">
						<div className="row">
							<div className="col-xs-12">
								<h2 className="tw">
									{team_1}
								</h2>
							</div>
						</div>
						<div className="row">
							<div className="col-xs-12">
								<p className="score tOne">
									{score_f1}
								</p>
							</div>
						</div>
						<div className="row mgn-top">
							<div className="col-xs-12 text-center">
								<button className="score-controls"
								        onClick={(event) => this.setState({score_1: this.state.score_1 + 1})}>+
								</button>
								<button className="score-controls"
								        onClick={(event) => this.setState({score_1: this.state.score_1 - 1})}>-
								</button>
							</div>
						</div>
					</div>
					<div className="col-xs-6">
						<div className="row">
							<div className="col-xs-12">
								<h2 className="tw">
									{team_2}
								</h2>
							</div>
						</div>
						<div className="row">
							<div className="col-xs-12">
								<p className="score tTwo">
									{score_f2}
								</p>
							</div>
						</div>
						<div className="row mgn-top">
							<div className="col-xs-12 text-center">
								<button className="score-controls"
								        onClick={(event) => this.setState({score_2: this.state.score_2 + 1})}>+
								</button>
								<button className="score-controls"
								        onClick={(event) => this.setState({score_2: this.state.score_2 - 1})}>-
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}

}
module.exports = Score;