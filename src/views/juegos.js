import React, {Component} from 'react';
import Score from './juegos/score';
import Matches from './juegos/matches';
import Stats from './juegos/stats';
import request from 'request';
import SocketClient from 'socket.io-client';

const socket = SocketClient.connect('http://labs.docademic.com:3006');

class Juegos extends Component {

	constructor(props) {
		super(props);
		this.state = {
			matches: [],
			teams: [],
			currentMatch: null
		};
	}

	componentDidMount() {
		this.getMatches();
		this.getTeams();
	}

	render() {
		return (
			<div className="full-scr" id="games">
				<div className="container">
					<Score
						currentMatch={this.state.currentMatch}
						getTeams={() => this.getTeams()}
						getMatches={() => this.getMatches()}
						finalizeMatch={() => {this.setState({currentMatch: null}); socket.emit('setMatch', null);}}
						socket={socket}/>
					<div className="row">
						<Matches matches={this.state.matches}
						         setCurrentMatch={match => {this.setState({currentMatch: match}); socket.emit('setMatch', match);}}/>
						<Stats
							teams={this.state.teams}
							resetStats={() => this.resetStats()}
							generateMatches={(ids) => this.generateMatches(ids)}/>
					</div>
				</div>
			</div>
		);
	}
	
	getMatches() {
		request.get({
			url: 'http://labs.docademic.com:3006/api/match',
			json: true,
			headers: {
				'Content-type': 'application/json',
				'Origin': 'http://localhost:3000'
			}
		}, (e, r, b) => {
			if (e) {
				console.error(e);
			} else {
				if (b.success) {
					this.setState({matches: b.data.matches});
				}
			}
		})
	}
	
	getTeams() {
		request.get({
			url: 'http://labs.docademic.com:3006/api/team',
			json: true,
			headers: {
				'Content-type': 'application/json',
				'Origin': 'http://localhost:3000'
			}
		}, (e, r, b) => {
			if (e) {
				console.error(e);
			} else {
				if (b.success) {
					this.setState({teams: b.data.teams});
				}
			}
		});
	}
	
	resetStats() {
		request.put({
			url: 'http://labs.docademic.com:3006/api/team/reset',
			json: true,
			headers: {
				'Content-type': 'application/json',
				'Origin': 'http://localhost:3000'
			}
		}, (e, r, b) => {
			if (e) {
				console.error(e);
			} else {
				if (b.success) {
					this.getTeams();
				}
			}
		})
	}
	
	generateMatches(selectedIds) {
		let requestData = {
			ids: selectedIds
		};
		request.post({
			url: 'http://labs.docademic.com:3006/api/match/generate',
			json: requestData,
			headers: {
				'Content-type': 'application/json',
				'Origin': 'http://localhost:3000'
			}
		}, (e, r, b) => {
			if (e) {
				console.error(e);
			} else {
				if (b.success) {
					this.getMatches();
				}
			}
		})
	}
}

module.exports = Juegos;