import React, {Component} from 'react';
import moment from 'moment';
import audio from '../../../public/audio/ring.mp3';
import request from 'request';
import SocketClient from 'socket.io-client';

const socket = SocketClient.connect('http://labs.docademic.com:3006');

class Score extends Component {

	constructor(props) {
		super(props);
		this.state = {
			currentMatch: null,
			score_1: 0,
			score_2: 0,
			cronM: '00',
			cronS: '00',
			audio: new Audio(audio)
		}
	}

	componentDidMount() {
		socket.on('setScore', (data) => {
			console.log(data);
			this.setState(data);
		});

	}

	calculateDif() {
		
		let dif = Date.now() - this.state.start_t;
		let time = moment(dif).format('mm:ss');
		let timer = time.split(':');
		if (timer[0] === '10' && timer[1] === '00') {
			this.endMatch();
		}
		this.setState({
			cronM: timer[0],
			cronS: timer[1]
		});
		
	}

	endMatch() {
		this.state.audio.play();
		clearInterval(this.state.interval);
		this.uploadMatch();
	}

	uploadMatch() {
		let id = this.props.currentMatch._id;
		let getTeams = this.props.getTeams;
		let getMatches = this.props.getMatches;
		let finalizeMatch = this.props.finalizeMatch;
		
		let requestData = {
			'score_1': this.state.score_1,
			'score_2': this.state.score_2
		}
		request.put({
			url: 'http://labs.docademic.com:3006/api/match/finish/' + id,
			json: requestData,
			headers: {
				'Content-type': 'application/json',
				'Origin': 'http://localhost:3000'
			}
		}, (e, r, b) => {
			if (e) {
				console.error(e);
			} else {
				if (b.success) {
					this.setState({
						start_t: null,
						in_match: false,
						cronM: '00',
						cronS: '00',
						score_1: 0,
						score_2: 0,
					});
					finalizeMatch();
					getTeams();
					getMatches();
				} else {
					alert("ocurrio un error al actualizar");
				}

			}
		});
	}

	startTimer() {
		let interval = setInterval(() => this.calculateDif(), 500);
		this.setState({
			start_t: Date.now(),
			in_match: true,
			interval: interval
		});
	}

	render() {
		let match = this.props.currentMatch;
		let score_f1 = this.state.score_1.toString().length === 1 ? '0' + this.state.score_1 : this.state.score_1;
		let score_f2 = this.state.score_2.toString().length === 1 ? '0' + this.state.score_2 : this.state.score_2;
		let team_1;
		let team_2;
		let button = <br/>;
		if (match) {
			team_1 = this.props.currentMatch.team_1.name;
			team_2 = this.props.currentMatch.team_2.name;
			button = this.state.in_match ?
				<button id="startT" onClick={(event) => this.endMatch()}>
					Finish
				</button> :
				<button id="startT" onClick={(event) => this.startTimer()}>
					start
				</button>
		} else {
			team_1 = 'HOME';
			team_2 = 'GUEST';
		}

		return (
			<div id="score" className="mgn-top">
				<div className="row">
					<div className="col-xs-4">
						<div className="row">
							<div className="col-xs-12">
								<h2 className="tw">
									{team_1}
								</h2>
							</div>
						</div>
						<div className="row">
							<div className="col-xs-12">
								<p className="score tOne">
									{score_f1}
								</p>
							</div>
						</div>
						<div className="row mgn-top">
							<div className="col-xs-12 text-center">
								<button className="score-controls"
								        onClick={(event) => this.setState({score_1: this.state.score_1 + 1})}>+
								</button>
								<button className="score-controls"
								        onClick={(event) => this.setState({score_1: this.state.score_1 - 1})}>-
								</button>
							</div>
						</div>
					</div>
					<div className="col-xs-4">
						<div id="timer" className="clearfix mgn-top">
							<div className="col-xs-3 col-xs-offset-2 clearfix">
								<p id="mins" className="ty text-right">
									{this.state.cronM}
								</p>
							</div>
							<div className="col-xs-1">
								<p className="ty">
									:
								</p>
							</div>
							<div className="col-xs-4">
								<p id="secs" className="ty">
									{this.state.cronS}
								</p>
							</div>
						</div>
						<div className="row mgn-top">
							<div className="col-xs-12">
								{button}
							</div>
						</div>
					</div>
					<div className="col-xs-4">
						<div className="row">
							<div className="col-xs-12">
								<h2 className="tw">
									{team_2}
								</h2>
							</div>
						</div>
						<div className="row">
							<div className="col-xs-12">
								<p className="score tTwo">
									{score_f2}
								</p>
							</div>
						</div>
						<div className="row mgn-top">
							<div className="col-xs-12 text-center">
								<button className="score-controls"
								        onClick={(event) => this.setState({score_2: this.state.score_2 + 1})}>+
								</button>
								<button className="score-controls"
								        onClick={(event) => this.setState({score_2: this.state.score_2 - 1})}>-
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
module.exports = Score;