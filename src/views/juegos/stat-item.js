import React, {Component} from 'react';

class Stat extends Component {
	render() {
		return (
			<div className="col-xs-11">
				<div className="col-xs-4">
					<p>
						{this.props.stat.name}
					</p>
				</div>
				<div className="col-xs-2">
					<p className="text-center">
						{this.props.stat.won}
					</p>
				</div>
				<div className="col-xs-2">
					<p className="text-center">
						{this.props.stat.lost}
					</p>
				</div>
				<div className="col-xs-2">
					<p className="text-center">
						{this.props.stat.score_positive}
					</p>
				</div>
				<div className="col-xs-2">
					<p className="text-center">
						{this.props.stat.score_negative}
					</p>
				</div>
			</div>
		);
	}
}

module.exports = Stat;