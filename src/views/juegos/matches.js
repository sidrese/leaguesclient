import React, {Component} from 'react';
import Match from './match';

class Games extends Component {

	render() {
		let setCurrentMatch = this.props.setCurrentMatch;
		let game = this.props.matches.map(function (match) {
			return (
				<Match key={match._id} game={match} setCurrentMatch={setCurrentMatch}/>
			);
		});
		return (
			<div className="col-xs-6">
				{game}
			</div>
		);
	}
}

module.exports = Games;