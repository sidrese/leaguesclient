import React, {Component} from 'react';
import Stat from './stat-item';

class Stats extends Component {
	
	constructor(props) {
		super(props);
		this.state = {
			ids: {}
		};
	}
	
	controlSelection(selectedId) {
		let newIds = this.state.ids;
		if (!this.state.ids[selectedId]) {
			newIds[selectedId] = true;
		} else {
			delete newIds[selectedId];
		}
		this.setState({ids: newIds});
	}
	
	getIds() {
		let ids = [];
		for (let attr in this.state.ids) {
			if(attr)
				ids.push(attr);
		}
		return ids;
	}
	
	render() {
		let stats = this.props.teams;
		let stat = stats.map((stat) => {
			return (
				<div className="row r-s" key={stat._id} onClick={(event) => this.controlSelection(stat._id)}>
					<div className="col-xs-1">
						<input type="checkbox" checked={this.state.ids[stat._id] ? true : false}/>
					</div>
					<Stat stat={stat}/>
				</div>
			);
		});
		return (
			<div className="col-xs-6">
				<div id="stats" className="mgn-top">
					<div id="stats-tools">
						<button
							onClick={(event) => this.props.generateMatches(this.getIds())}>Generate Matches
						</button>
						<button
							onClick={(event) => this.props.resetStats()}>Reset
						</button>
					</div>
					<div id="stats-header" className="clearfix">
						<div className="col-xs-4">
							<p className="text-center">
								Equipo
							</p>
						</div>
						<div className="col-xs-2">
							<p className="text-center">
								Won
							</p>
						</div>
						<div className="col-xs-2">
							<p className="text-center">
								Lost
							</p>
						</div>
						<div className="col-xs-2">
							<p className="text-center">
								SP
							</p>
						</div>
						<div className="col-xs-2">
							<p className="text-center">
								SN
							</p>
						</div>
					</div>
					<div id="stats-container" className="clearfix">
						{stat}
					</div>
				</div>
			</div>
		);
	}
}

module.exports = Stats;