import React, {Component} from 'react';

class Games extends Component {

	render() {
		let clases = this.props.game.finalized?"shadowbox finished text-center mgn-top ":"shadowbox text-center mgn-top ";
		return (
			<div className={clases}
			     onClick={(event) => {if(!this.props.game.finalized)this.props.setCurrentMatch(this.props.game)}}>
				<div className="row">
					<div className="col-xs-4">
						<p>
							{this.props.game.team_1.name}
						</p>
						<ul className="mgn-top">
							<li>
								{this.props.game.team_1.players[0].name}
							</li>
							<li>
								{this.props.game.team_1.players[1].name}
							</li>
						</ul>
					</div>
					<div className="col-xs-1">
						<p>
							{this.props.game.score_1}
						</p>
					</div>
					<div className="col-xs-2">
						<p>
							VS
						</p>
					</div>
					<div className="col-xs-1">
						<p>
							{this.props.game.score_2}
						</p>
					</div>
					<div className="col-xs-4">
						<p>
							{this.props.game.team_2.name}
						</p>
						<ul className="mgn-top">
							<li>
								{this.props.game.team_2.players[0].name}
							</li>
							<li>
								{this.props.game.team_2.players[1].name}
							</li>
						</ul>
					</div>
				</div>
			</div>
		);
	}
}

module.exports = Games;