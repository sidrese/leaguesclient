import React, {Component} from 'react';
import CONST from '../util/constants';
import Effects from '../util/effects';
import {Link} from 'react-router';
import request from 'request';

class Landing extends Component {

	constructor(props) {
		super(props);
		this.state = {
			team: '',
			player1: '',
			player2: ''
		};
	}

	componentDidMount() {
		Effects.setuptoggle();
	}

	registerTeam() {

		if (this.state.team !== '' && this.state.player1 !== '' && this.state.player2 !== '') {
			let requestData = {
				name: this.state.team,
				players: [
					{name: this.state.player1},
					{name: this.state.player2}
				]
			};
			request.post({
				url: 'http://labs.docademic.com:3006/api/team',
				json: requestData,
				headers: {
					'Content-type': 'application/json',
					'Origin': 'http://localhost:3000'
				}
			}, function (e, r, b) {
				if (e) {
					console.error(e);
				} else {
					console.log(b);
					if (b.success) {
						alert('Equipo Agregado');
						console.log(this);
						this.setState({team: '', player1: '', player2: ''});
						Effects.toggle();
					} else {
						alert(b.message);
					}
				}
			}.bind(this))
		} else {
			console.log("Empty field");
		}
	}

	render() {
		return (
			<div className="full-scr" id="landing">
				<div className="vert-text">
					<div className="container">
						<div id="logo-container">
							<img
								src={CONST.imgs.logo.src}
								alt={CONST.imgs.logo.alt}
							/>
						</div>
						<div className="row">
							<div className="col-xs-6 col-md-2 col-md-offset-4">
								<button className="register">
									Registro
								</button>
							</div>
							<div className="col-xs-6 col-md-2">
								<Link to={`/juegos`}>
									Juegos
								</Link>
							</div>
						</div>
					</div>
				</div>
				<div id="register-bg">
					<div id="register-container">
						<form action="">
							<div className="row">
								<div className="col-lg-12">
									<input type="text" placeholder="Nombre equipo"
									       value={this.state.team}
									       onChange={event => {
										       this.setState({team: event.target.value});
									       }}/>
								</div>


								<div className="col-lg-6">
									<input type="text" placeholder="Nombre 1"
									       value={this.state.player1}
									       onChange={event => {
										       this.setState({player1: event.target.value});
									       }}/>
								</div>
								<div className="col-lg-6">
									<input type="text" placeholder="Nombre 2"
									       value={this.state.player2}
									       onChange={event => {
										       this.setState({player2: event.target.value});
									       }}/>
								</div>
							</div>
							<div className="row clearfix">
								<div className="col-lg-12">
									<div className="pull-right">
										<button onClick={(event) => {
											event.preventDefault();
											this.registerTeam()
										}}>
											Registrar
										</button>
									</div>
									<div className="pull-right">
										<button className="register">
											Cancelar
										</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		);
	}
}

module.exports = Landing;