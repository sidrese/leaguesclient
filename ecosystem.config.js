module.exports = {

	apps: [

		// First application
		{
			name: "Leagues Client",
			script: "deploy.js",
			env: {
				NODE_ENV: "development"
			},
			env_production: {
				NODE_ENV: "production"
			},
			env_labs: {
				NODE_ENV: "labs"
			}
		}
	],


	deploy: {
		/*production : {
		 user : "node",
		 host : "212.83.163.1",
		 ref  : "origin/master",
		 repo : "git@github.com:repo.git",
		 path : "/var/www/production",
		 "post-deploy" : "npm install && pm2 startOrRestart ecosystem.json --env production"
		 },*/
		labs: {
			user: "deploy",
			host: "54.208.239.0",
			ref: "origin/develop",
			repo: "https://enrique_si:Quique_58@bitbucket.org/sidrese/leaguesclient.git",
			path: "/home/deploy/leagues/client",
			"post-deploy": "npm install && npm run build && pm2 startOrRestart ecosystem.config.js --env dev",
			env: {
				NODE_ENV: "labs",
				PORT: 3004
			}
		}
	}
}
